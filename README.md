# Resume
ACADEMIC PROFILE

YEAR              DEGREE             INSTITUTE                    PERFORMANCE
2020(Expected)    Computer Science   BVRIT Hyderabad                8.46(4 Semester)
                   and Engineering   College of Enginering For
                                      Women


PROJECTS UNDERTAKEN:

SCRABBLE PAL in Haskell
Python Tutor using Django and Python: its a replica of the existing python tutor
A project based on advanced java

TECHNICAL SKILLS:

Languages:  C, C++, Java, Haskell, HTML, Python,Django
Platforms: Windows, Linux( Ubuntu,Debian)
Relevant Courses: Fundamentals of Computing, Data Structures and Algorithms, Operating Systems,Database Management.
Courses currently doing:  Machine learning and Data Communication and networks

PERSONAL INFORMATION

Alternate email id:    		varsha.sirvee@gmail.com

